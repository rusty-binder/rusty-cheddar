extern crate binder;
extern crate cheddar;

use binder::compiler::test;

macro_rules! inner_cheddar_cmp {
    ($name:ident $( #[$attr:meta] )*, $binder:expr, $header:expr, $std:expr) => {
        #[test]
        $( #[$attr] )*
        fn $name() {
            let expected = test::binding_from_file_contents(
                &format!("c-header-{:?}", $std),
                concat!(stringify!($name), ".h"),
                concat!("#define cheddar_generated_", stringify!($name), "_h\n", $header),
            );

            let mut binder = $binder;

            let actual = match binder.compile() {
                Ok(b) => b,
                Err(_) => panic!("compilation errors:\n{}", binder.errors()),
            };

            test::cmp_bindings_with(&expected, &actual[0], &|expected, actual| {
                let cmp_script = std::env::current_dir()
                    .map(|p| p.join("tests/cmp_header.py"))
                    .expect("internal testing error: unable to find current directory");

                let output = std::process::Command::new(&cmp_script)
                    .arg(&expected)
                    .arg(&actual)
                    .output()
                    .expect("internal testing error: could not run `cmp_header.py`");

                if !output.status.success() {
                    if !output.stderr.is_empty() {
                        panic!(
                            "internal testing error: `cmp_header.py` failed: {}",
                            String::from_utf8_lossy(&output.stderr),
                        );
                    } else {
                        panic!("{}: {}", String::from_utf8_lossy(&output.stdout), actual);
                    }
                }
            });
        }
    };
}

/// Compares a generated header file to an expected one using `cmp_header.py`.
///
/// Do not put any boiler plate in the strings, only put the items you want to test.
///
/// For the rust file do not omit anything.
///
/// For the header file omit:
///
/// ```C
/// #ifndef cheddar_gen_cheddar_h
/// #define cheddar_gen_cheddar_h
///
/// #ifdef __cplusplus
/// extern "C" {
/// #endif
///
/// #include <stdnoreturn.h>
/// #include <stdint.h>
/// #include <stdbool.h>
///
/// ...
///
/// #ifdef __cplusplus
/// }
/// #endif
///
/// #endif
/// ```
macro_rules! cheddar_cmp {
    ($name:ident $( #[$attr:meta] )*, $header:expr, $rust:expr) => {
        inner_cheddar_cmp! {
            $name $( #[$attr] )*,
            {
                let mut binder = binder::Binder::new().expect("could not read cargo manifest");
                binder.source_string($rust);
                let compiler = cheddar::Header::c99()
                    .name(stringify!($name));
                binder.register(compiler);
                binder
            },
            $header, cheddar::Standard::C99
        }
    };

    ($name:ident $( #[$attr:meta] )*, custom $custom:expr, $header:expr, $rust:expr) => {
        inner_cheddar_cmp! {
            $name $( #[$attr] )*,
            {
                let mut binder = binder::Binder::new().expect("could not read cargo manifest");
                binder.source_string($rust);
                let compiler = cheddar::Header::c99()
                    .name(stringify!($name))
                    .insert_code($custom);
                binder.register(compiler);
                binder
            },
            $header, cheddar::Standard::C99
        }
    };

    ($name:ident $( #[$attr:meta] )*, api $api:expr, $header:expr, $rust:expr) => {
        inner_cheddar_cmp! {
            $name $( #[$attr] )*,
            {
                let mut binder = binder::Binder::new().expect("could not read cargo manifest");
                binder.source_string($rust);
                binder.module($api).expect("malformed module path");
                let compiler = cheddar::Header::c99()
                    .name(stringify!($name));
                binder.register(compiler);
                binder
            },
            $header, cheddar::Standard::C99
        }
    };

    ($name:ident $( #[$attr:meta] )*, no_namespace_enums, $header:expr, $rust:expr) => {
        inner_cheddar_cmp! {
            $name $( #[$attr] )*,
            {
                let mut binder = binder::Binder::new().expect("could not read cargo manifest");
                binder.source_string($rust);
                let compiler = cheddar::Header::c99()
                    .name(stringify!($name))
                    .namespace_enums(false);
                binder.register(compiler);
                binder
            },
            $header, cheddar::Standard::C99
        }
    };

    ($name:ident $( #[$attr:meta] )*, std = $std:expr, $header:expr, $rust:expr) => {
        inner_cheddar_cmp! {
            $name $( #[$attr] )*,
            {
                let mut binder = binder::Binder::new().expect("could not read cargo manifest");
                binder.source_string($rust);
                let compiler = cheddar::Header::with_standard($std)
                    .name(stringify!($name))
                    .namespace_enums(false);
                binder.register(compiler);
                binder
            },
            $header, $std
        }
    };
}


// TYPEDEFS

cheddar_cmp! { typedefs,
    "
    typedef int64_t Int64;
    ",
    "
    pub type Int64 = i64;
    // Shouldn't appear in output header file.
    type Float32 = f32;
    // Shouldn't appear in output header file.
    pub type AResult<T> = Result<T, ()>;
    "
}


// ENUMS

cheddar_cmp! { compilable_enums,
    "
    typedef enum Colours {
        Colours_Red,
        Colours_Orange,
        Colours_Yellow,
        Colours_Green,
        Colours_Blue,
        Colours_Indigo,
        Colours_Violet,
    } Colours;

    typedef enum TypesOfLabrador {
        TypesOfLabrador_Stupid = -8,
        TypesOfLabrador_Braindead,
    } TypesOfLabrador;
    ",
    "
    #[repr(C)]
    pub enum Colours {
        Red,
        Orange,
        Yellow,
        Green,
        Blue,
        Indigo,
        Violet,
    }

    // Shouldn't appear in the output header file.
    #[allow(dead_code)]
    #[repr(C)]
    enum Planets {
        Mercury,
        Venus,
        Earth,
        Mars,
        Jupiter,
        Saturn,
        Neptune,
        // TODO: campaign in support of Pluto!
    }

    // Shouldn't appear in the output header file.
    pub enum Days {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
    }

    #[repr(C)]
    pub enum TypesOfLabrador {
        Stupid = -8,
        Braindead,
    }
    "
}

cheddar_cmp! { no_namespace_enums, no_namespace_enums,
    "
    typedef enum Foo {
        A,
        B,
    } Foo;
    ",
    "
    #[repr(C)]
    pub enum Foo {
        A,
        B,
    }
    "
}

cheddar_cmp! { incompilable_generic_enums
    #[should_panic(expected = "binder can not handle parameterized `#[repr(C)]` enums")],
    "
    typedef enum Foo {
        Foo_A,
        Foo_B,
    } Foo;
    ",
    "
    #[repr(C)]
    pub enum Foo<T> {
        A(T),
        B,
    }
    "
}

cheddar_cmp! { incompilable_tuple_enums
    #[should_panic(expected = "non-unit enum variants have no C equivalent")],
    "
    typedef enum Bar {
        Bar_Baz,
        Bar_Bux,
    } Bar;
    ",
    "
    #[repr(C)]
    pub enum Bar {
        Baz(i32),
        Bux,
    }
    "
}

cheddar_cmp! { incompilable_struct_enums
    #[should_panic(expected = "non-unit enum variants have no C equivalent")],
    "
    typedef enum Foo {
        Foo_Bar,
        Foo_Baz,
    } Foo;
    ",
    "
    #[repr(C)]
    pub enum Foo {
        Bar { bax: i32 },
        Baz,
    }
    "
}


// STRUCTS

cheddar_cmp! { compilable_structs,
    "
    typedef struct Student {
        int32_t id;
        int32_t roll;
        double score;
    } Student;

    typedef struct Vector {
        double _0;
        double _1;
        double _2;
    } Vector;
    ",
    "
    #[repr(C)]
    pub struct Student {
        pub id: i32,
        roll: i32,
        score: f64,
    }

    #[repr(C)]
    pub struct Vector(pub f64, f64, f64);

    // Shouldn't appear in the output header file.
    #[allow(dead_code)]
    #[repr(C)]
    struct Complex {
        pub real: f64,
        imag: f64,
    }

    // Shouldn't appear in output header file.
    pub struct Employee {
        pub id: i32,
        age: i16,
        salary: f64,
    }
    "
}

cheddar_cmp! { opaque_structs,
    "
    typedef struct Foo Foo;

    typedef struct Bar Bar;
    ",
    "
    #[repr(C)]
    pub struct Foo {
        _inner: Vec<Option<i32>>,
    }

    #[repr(C)]
    pub struct Bar(String, Vec<i32>, PathBuf);
    "
}

cheddar_cmp! { incompilable_unit_structs
    #[should_panic(expected = "binder can not handle unit `#[repr(C)]` structs")],
    "
    typedef struct Foo Foo;
    ",
    "
    #[repr(C)]
    pub struct Foo;
    "
}


// FUNCTIONS

cheddar_cmp! { compilable_functions,
    "
    int64_t add_i64(int64_t lhs, int64_t rhs);

    void takes_no_arguments(void);

    void pattern_in_arg(int32_t a);
    ",
    r#"
    #[no_mangle]
    pub extern fn takes_no_arguments() {
        println!("hi");
    }

    #[no_mangle]
    pub extern fn pattern_in_arg(mut a: i32) {
        a += 1;
    }

    #[no_mangle]
    pub extern fn add_i64(lhs: i64, rhs: i64) -> i64 {
        lhs + rhs
    }


    // Shouldn't appear in the output header file.
    #[allow(dead_code)]
    #[no_mangle]
    extern fn add_i32(lhs: i32, rhs: i32) -> i32 {
        lhs + rhs
    }

    // Shouldn't appear in output header file.
    #[no_mangle]
    pub fn add_i16(lhs: i16, rhs: i16) -> i16 {
        lhs + rhs
    }

    // Shouldn't appear in output header file.
    pub extern fn add_i8(lhs: i8, rhs: i8) -> i8 {
        lhs + rhs
    }
    "#
}

cheddar_cmp! { compilable_function_pointers,
    "
    typedef const int32_t** (*TwoIntPtrFnPtr)(double* argument);

    typedef int32_t (*VoidFunc)(void);

    double cmp(double (*cmp_fn)(double lhs, double rhs), double lhs, double rhs);

    typedef bool (*Foo)(double, double);

    void (*signal(int sig, void (*func)(int)))(int);
    ",
    r#"
    pub type TwoIntPtrFnPtr = extern fn(argument: *mut f64) -> *const *mut i32;

    pub type VoidFunc = extern fn() -> i32;

    #[no_mangle]
    pub extern fn cmp(cmp_fn: extern fn(lhs: f64, rhs: f64) -> f64, lhs: f64, rhs: f64) -> f64 {
        cmp_fn(lhs, rhs)
    }

    pub type Foo = extern fn(f64, f64) -> bool;

    #[no_mangle]
    pub extern fn signal(sig: libc::c_int, func: extern fn(libc::c_int)) -> extern fn(libc::c_int) {
        println!("{}", sig);
        func
    }
    "#
}


// TYPES

cheddar_cmp! { pure_rust_types,
    "
    typedef void MyVoid;
    typedef float Float32;
    typedef double Float64;
    typedef int8_t Int8;
    typedef int16_t Int16;
    typedef int32_t Int32;
    typedef int64_t Int64;
    typedef intptr_t Int;
    typedef uint8_t UInt8;
    typedef uint16_t UInt16;
    typedef uint32_t UInt32;
    typedef uint64_t UInt64;
    typedef uintptr_t UInt;
    typedef bool Bool;
    typedef double* FloatArray;
    typedef bool const* LogicArray;
    typedef int32_t**** FourPointers;
    typedef float const* const* TwoPointers;
    ",
    "
    pub type MyVoid = ();
    pub type Float32 = f32;
    pub type Float64 = f64;
    pub type Int8 = i8;
    pub type Int16 = i16;
    pub type Int32 = i32;
    pub type Int64 = i64;
    pub type Int = isize;
    pub type UInt8 = u8;
    pub type UInt16 = u16;
    pub type UInt32 = u32;
    pub type UInt64 = u64;
    pub type UInt = usize;
    pub type Bool = bool;
    pub type FloatArray = *mut f64;
    pub type LogicArray = *const bool;
    pub type FourPointers = *mut *mut *mut *mut i32;
    pub type TwoPointers = *const *const f32;
    "
}

cheddar_cmp! { libc_types,
    "
    typedef void CVoid;
    typedef float CFloat;
    typedef double CDouble;
    typedef char CChar;
    typedef signed char CSChar;
    typedef unsigned char CUChar;
    typedef short CShort;
    typedef unsigned short CUShort;
    typedef int CInt;
    typedef unsigned int CUInt;
    typedef long CLong;
    typedef unsigned long CULong;
    typedef long long CLongLong;
    typedef unsigned long long CULongLong;
    typedef FILE CFile;
    ",
    "
    pub type CVoid = libc::c_void;
    pub type CFloat = libc::c_float;
    pub type CDouble = libc::c_double;
    pub type CChar = libc::c_char;
    pub type CSChar = libc::c_schar;
    pub type CUChar = libc::c_uchar;
    pub type CShort = libc::c_short;
    pub type CUShort = libc::c_ushort;
    pub type CInt = libc::c_int;
    pub type CUInt = libc::c_uint;
    pub type CLong = libc::c_long;
    pub type CULong = libc::c_ulong;
    pub type CLongLong = libc::c_longlong;
    pub type CULongLong = libc::c_ulonglong;
    pub type CFile = libc::FILE;
    "
}

cheddar_cmp! { libc_import_types,
    "
    typedef void CVoid;
    typedef float CFloat;
    typedef double CDouble;
    typedef char CChar;
    typedef signed char CSChar;
    typedef unsigned char CUChar;
    typedef short CShort;
    typedef unsigned short CUShort;
    typedef int CInt;
    typedef unsigned int CUInt;
    typedef long CLong;
    typedef unsigned long CULong;
    typedef long long CLongLong;
    typedef unsigned long long CULongLong;
    typedef FILE CFile;
    ",
    "
    use libc::*;

    pub type CVoid = c_void;
    pub type CFloat = c_float;
    pub type CDouble = c_double;
    pub type CChar = c_char;
    pub type CSChar = c_schar;
    pub type CUChar = c_uchar;
    pub type CShort = c_short;
    pub type CUShort = c_ushort;
    pub type CInt = c_int;
    pub type CUInt = c_uint;
    pub type CLong = c_long;
    pub type CULong = c_ulong;
    pub type CLongLong = c_longlong;
    pub type CULongLong = c_ulonglong;
    pub type CFile = FILE;
    "
}

cheddar_cmp! { std_os_raw_types,
    "
    typedef void CVoid;
    typedef char CChar;
    typedef double CDouble;
    typedef float CFloat;
    typedef int CInt;
    typedef long CLong;
    typedef long long CLongLong;
    typedef signed char CSChar;
    typedef short CShort;
    typedef unsigned char CUChar;
    typedef unsigned int CUInt;
    typedef unsigned long CULong;
    typedef unsigned long long CULongLong;
    typedef unsigned short CUShort;
    ",
    "
    pub type CVoid = std::os::raw::c_void;
    pub type CChar = std::os::raw::c_char;
    pub type CDouble = std::os::raw::c_double;
    pub type CFloat = std::os::raw::c_float;
    pub type CInt = std::os::raw::c_int;
    pub type CLong = std::os::raw::c_long;
    pub type CLongLong = std::os::raw::c_longlong;
    pub type CSChar = std::os::raw::c_schar;
    pub type CShort = std::os::raw::c_short;
    pub type CUChar = std::os::raw::c_uchar;
    pub type CUInt = std::os::raw::c_uint;
    pub type CULong = std::os::raw::c_ulong;
    pub type CULongLong = std::os::raw::c_ulonglong;
    pub type CUShort = std::os::raw::c_ushort;
    "
}

// Verify we don't accept module types without a full prefix.
cheddar_cmp! { module_no_prefix #[should_panic],
    "
    typedef void CVoid;
    typedef FILE CFILE;
    ",
    "
    use libc::FILE;
    use std::os::raw;
    pub type CVoid = raw::c_void;
    pub type CFile = FILE;
    "
}


// MODULE

cheddar_cmp! { module, api "api",
    "
    typedef float Float;
    ",
    "
    pub use api::*;
    mod api {
        pub type Float = f32;
    }
    "
}

cheddar_cmp! { inside_module, api "c::api",
    "
    typedef float Float;
    ",
    "
    pub use c::api::*;
    mod c {
        mod api {
            pub type Float = f32;
        }
    }
    "
}


// MISC

cheddar_cmp! { custom,
    custom "
    typedef F64 MyF64;
    ",
    "
    typedef double F64;
    typedef F64 MyF64;
    ",
    "
    pub type F64 = f64;
    "
}

cheddar_cmp! { general_interplay,
    "
    typedef float Kg;
    typedef float Lbs;
    typedef float M;
    typedef float Ins;

    typedef enum Eye {
        Eye_Blue = -1,
        Eye_Green,
        Eye_Red,
    } Eye;

    typedef struct Person {
        int8_t age;
        Eye eyes;
        Kg weight;
        M height;
    } Person;

    Person Person_create(int8_t age, Eye eyes, float weight_lbs, float height_ins);
    void Person_describe(Person person);
    ",
    r#"
    pub type Kg = f32;
    pub type Lbs = f32;
    pub type M = f32;
    pub type Ins = f32;

    #[repr(C)]
    pub enum Eye {
        Blue = -1,
        Green,
        Red,
    }

    #[repr(C)]
    pub struct Person {
        pub age: i8,
        eyes: Eye,
        weight: Kg,
        height: M,
    }

    #[allow(non_snake_case)]
    #[no_mangle]
    pub extern fn Person_create(age: i8, eyes: Eye, weight_lbs: f32, height_ins: f32) -> Person {
        Person {
            age: age,
            eyes: eyes,
            weight: weight_lbs * 0.45,
            height: height_ins * 0.0254,
        }
    }

    #[allow(non_snake_case)]
    #[no_mangle]
    pub extern fn Person_describe(person: Person) {
        let eyes = match person.eyes {
            Eye::Blue => "blue",
            Eye::Green => "green",
            Eye::Red => "red",
        };
        println!(
            "The {}m {} year old weighed {}kg and had {} eyes.",
            person.height, person.age, person.weight, eyes,
        );
    }
    "#
}

cheddar_cmp! { no_return,
    std = ::cheddar::Standard::C11,
    "
    noreturn void panic(void);
    ",
    "
    #[no_mangle]
    pub extern fn panic() -> ! {
        panic!()
    }
    "
}
