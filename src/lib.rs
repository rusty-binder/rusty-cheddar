//! Rusty-cheddar is a crate for converting Rust source files into C header files.
//!
//! **A note on versioning:** While rusty-cheddar is still in a significant flux (i.e.
//! pre-`v1.0.0`) it will likely go through numerous breaking changes. However, until `v1.0.0`, any
//! time a breaking change is made the minor version will be bumped and any time a new feature is
//! added the path version will be bumped.
//!
//! Rusty-cheddar examines your Rust source code and produces a header file of the bits which are C
//! compatible. See the documentation for `Generator` to get a better understanding of how this is
//! done.
//!
//! The most useful way to use rusty-cheddar is in a build script. This can either be done using
//! the `run_build` convenience method provided in rusty-cheddar, or by using the more general
//! [rusty-binder] framework.
//!
//! # rusty-cheddar
//!
//! If the only bindings you want are a C header file then it makes sense to just rely on
//! [rusty-cheddar]. First you should set up the `Builder` type with your required settings, then
//! just call the `run_build` method to create the files.
//!
//! ```toml
//! # Cargo.toml
//!
//! [package]
//! build = "build.rs"
//!
//! [build-dependencies]
//! rusty-cheddar = "..."
//!
//! [lib]
//! crate-type = ["dylib"]
//! ```
//!
//! ```no_run
//! // build.rs
//!
//! extern crate cheddar;
//!
//! fn main() {
//!     cheddar::Builder::c99().expect("could not read cargo manifest")
//!         .name("my_awesome_header.h")
//!         .insert_code("// This header is automatically generated - DO NOT CHANGE IT MANUALLY!")
//!         .output_directory("target/include")
//!         .module("c_api").expect("malformed module path")
//!         .run_build();
//! }
//! ```
//!
//! # rusty-binder
//!
//! If you want to compile bindings for multiple languages, or you require more fine-grained
//! control, then you must hook into the more general [rusty-binder] framework.
//!
//! ```toml
//! # Cargo.toml
//!
//! [package]
//! build = "build.rs"
//!
//! [build-dependencies]
//! rusty-binder = "..."
//! rusty-cheddar = "..."
//!
//! [lib]
//! crate-type = ["dylib"]
//! ```
//!
//! ```no_run
//! // build.rs
//!
//! extern crate binder;
//! extern crate cheddar;
//!
//! fn main() {
//!      let mut c99 = cheddar::Header::c99()
//!          .name("my_header_c99.h");
//!
//!      let mut c89 = cheddar::Header::with_standard(cheddar::Standard::C89)
//!          .name("my_header_c89.h");
//!
//!      binder::Binder::new().expect("could not read cargo manifest")
//!          .module("capi").expect("malformed module path")
//!          .output_directory("target/include")
//!          .register(c99)
//!          .register(c89)
//!          .run_build();
//! }
//! ```
//!
//!
//! [rusty-binder]: https://gitlab.com/rusty-binder/rusty-binder
//! [rusty-cheddar]: https://gitlab.com/rusty-binder/rusty-cheddar

extern crate binder;
extern crate itertools;

use binder::compiler;
use binder::compiler::utils;
use binder::compiler::session;
use binder::compiler::syntax;

mod types;
pub use self::types::CustomTypeModule;

/// The C standard to which the header file should conform.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Standard {
    /// Conform to the C89 standard.
    ///
    /// This will only use `/* ... */` style comments and will mark diverging functions as `void`.
    C89,
    /// Conform to the C99 standard.
    ///
    /// This will include `stdint.h` and `stdbool.h` to allow fized-width integers and booleans but
    /// will mark diverging functions as `void`.
    C99,
    /// Conform to the C11 standard.
    ///
    /// This will include `stdint.h` and `stdbool.h` to allow fixed-width integers and booleans and
    /// will include `stdnoreturn.h` to mark diverging functions as `noreturn`.
    C11,
}

impl Standard {
    /// Generate the required includes for the standard.
    fn includes(&self) -> String {
        let mut include_string = String::new();

        if *self >= Standard::C11 {
            include_string.push_str("#include <stdnoreturn.h>\n");
        }

        if *self >= Standard::C99 {
            include_string.push_str("#include <stdint.h>\n#include <stdbool.h>\n");
        }

        include_string
    }
}


/// The struct responsible for compiling the header file.
///
/// # Conversions
///
/// In the examples below, boilerplate has been omitted from the header.
///
/// ## Typedefs
///
/// rusty-cheddar converts `pub type A = B` into `typedef B A;`. Types containing generics are ignored.
///
/// Rust:
///
/// ```ignore
/// type UInt32 = u32;
/// pub type UInt64 = u64;
/// pub type MyOption<T> = Option<T>
/// ```
///
/// Header:
///
/// ```C
/// typedef uint64_t UInt64;
/// ```
///
/// ## Enums
///
/// rusty-cheddar will convert public enums which are marked `#[repr(C)]`. If the enum is generic or
/// contains tuple or struct variants then `cheddar` will fail. rusty-cheddar should correctly handle
/// explicit discriminants.
///
/// Rust:
///
/// ```ignore
/// #[repr(C)]
/// pub enum Colours {
///     Red = -6,
///     Blue,
///     Green = 7,
///     Yellow,
/// }
///
/// // This would fail is it was #[repr(C)].
/// pub enum Tastes<T> {
///     Savoury(T),
///     Sweet,
/// }
///
/// // This would fail if it was public.
/// #[repr(C)]
/// enum Units {
///     Kg(f64),
///     M(f64),
///     S(f64),
///     A(f64),
///     K(f64),
///     Mol(f64),
///     Cd(f64),
/// }
/// ```
///
/// Header:
///
/// ```C
/// typedef enum Colours {
///         Red = -6,
///         Blue,
///         Green = 7,
///         Yellow,
/// } Colours;
/// ```
///
/// ## Structs
///
/// Structs are handled very similarly to enums, they must be public, marked `#[repr(C)]`, and they
/// must not contain generics (this is currently only checked at the struct-level, generic fields
/// are not checked). Structs which contain at least one public member will be written to the
/// header in it's entirety.
///
/// Rust:
///
/// ```ignore
/// #[repr(C)]
/// pub struct Person {
///     age: i32,
///     pub height: f64,
///     weight: f64,
/// }
/// ```
///
/// Header:
///
/// ```C
/// typedef struct Person {
///         int32_t age;
///         double height;
///         double weight;
/// } Person;
/// ```
///
/// ### Opaque Structs
///
/// One common C idiom is to hide the implementation of a struct using an opaque struct, which can
/// only be used behind a pointer. This is especially useful in Rust-C interfaces as it allows you
/// to use _any arbitrary Rust struct_ in C.
///
/// To define an opaque struct you simple define a struct whose members are all private.
///
/// Rust:
///
/// ```ignore
/// struct Foo<T> {
///     bar: i32,
///     baz: Option<T>,
/// }
///
/// #[repr(C)]
/// pub struct MyCrate_Foo {
///     _inner: Foo<PathBuf>,
/// }
/// ```
///
/// Header:
///
/// ```C
/// typedef struct MyCrate_Foo MyCrate_Foo;
/// ```
///
/// Note that the struct _must not_ be generic but the type that it wraps can be arbitrary.
///
/// ## Functions
///
/// For rusty-cheddar to pick up on a function declaration it must be public, marked `#[no_mangle]` and
/// have one of the following ABIs:
///
/// - C
/// - Cdecl
/// - Stdcall
/// - Fastcall
/// - System
///
/// I'm not totally up to speed on calling conventions so if you believe one of these has been including
/// in error, or if one has been omitted, then please open an issue at the [repo].
///
/// Rust:
///
/// ```ignore
/// use std::ops::Add;
///
/// #[no_mangle]
/// pub extern fn hello() {
///     println!("Hello!");
/// }
///
/// fn add<O, R, L: Add<R, Output=O>>(l: L, r: R) -> O {
///     l + r
/// }
///
/// #[no_mangle]
/// #[allow(non_snake_case)]
/// pub extern fn MyAdd_add_u8(l: u8, r: u8) -> u8 {
///     add(l, r)
/// }
///
/// #[no_mangle]
/// #[allow(non_snake_case)]
/// pub extern fn MyAdd_add_u16(l: u16, r: u16) -> u16 {
///     add(l, r)
/// }
/// ```
///
/// Header:
///
/// ```C
/// void hello();
///
/// uint8_t MyAdd_add_u8(uint8_t l, uint8_t r);
///
/// uint16_t MyAdd_add_u16(uint16_t l, uint16_t r);
/// ```
///
/// ## Paths
///
/// You must not put types defined in other modules in an exported type signature without hiding it
/// behind an opaque struct. This is because the C compiler must know the layout of the type and
/// rusty-cheddar will not search other modules by default.
///
/// The very important exception to this rule are the C ABI types defined in
/// the `libc` crate and `std::os::raw`.
///
/// If absolute needed it is also possible to add more custom modules.
///
/// ```no_run
/// // build.rs
/// extern crate binder;
/// extern crate cheddar;
///
///
/// fn custom_convert(t: &str) -> &str {
///     // your convert code
///     t
/// }
///
/// fn main() {
///     let header = cheddar::Header::c99()
///                      .register_custom_module("my::module", custom_convert);
///
///     binder::Binder::new().expect("could not read manifest")
///         .register(header)
///         .module("c_api").expect("malformed module path")
///         .run_build();
/// }
/// ```
///
/// [repo]: https://gitlab.com/rusty-binder/rusty-cheddar
pub struct Header {
    /// The name to give the header file.
    name: String,
    /// Which standard the header file should conform to.
    standard: Standard,
    /// Storage for the header file.
    buffer: String,
    /// Custom C code which is placed after the `#include`s.
    custom_code: String,
    /// Whether to prepend enum variants with the name of the enum.
    namespace_enums: bool,
    type_factory: types::TypeFactory,
}

impl Header {
    /// Initialise a compiler which will compile a header file conforming to the C99 standard.
    pub fn c99() -> Header {
        Self::with_standard(Standard::C99)
    }

    /// Initialise a compiler which will compile a header file conforming to a given standard.
    pub fn with_standard(standard: Standard) -> Header {
        Header {
            name: "header.h".into(),
            standard: standard,
            buffer: String::new(),
            custom_code: String::new(),
            namespace_enums: true,
            type_factory: types::TypeFactory::new(Vec::new(), Vec::new()),
        }
    }

    /// Register a custom type module for type resultion
    pub fn register_custom_module<F>(mut self, mod_name: &'static str, convert: F) -> Header
        where F: Fn(&str) -> &str +'static
    {
        self.type_factory.custom_type_modules.push(self::types::CustomTypeModule::new(mod_name, convert));
        self.type_factory.custom_type_module_names.push(mod_name.to_owned());
        self
    }

    /// Give the header file a name.
    ///
    /// This name will be used in the include-guard and will be used to generate the name of the
    /// header file itself.
    pub fn name(mut self, name: &str) -> Header {
        self.name = name.into();
        self
    }

    /// Inject custom code before the generated decalrations.
    ///
    /// The rough structure of the header file will be
    ///
    /// ```c
    /// #ifndef ...
    /// #define ...
    ///
    /// #ifdef ...
    /// extern "C" {
    /// #endif
    ///
    /// #include ...
    ///
    /// <custom code goes here>
    ///
    /// <generated code goes here>
    ///
    /// #ifdef ...
    /// }
    /// #endif
    ///
    /// #endif
    /// ```
    pub fn insert_code(mut self, code: &str) -> Header {
        self.custom_code.push_str(code);
        self
    }

    /// Prepend the name of enums and an underscore to their variants.
    ///
    /// Defaults to `true`.
    ///
    /// This prevents name clashes in the C code which don't happen in the Rust code due to
    /// namespacing.
    ///
    /// When `true`
    ///
    /// ```no_run
    /// #[repr(C)]
    /// pub enum Foo {
    ///     Bar,
    ///     Baz,
    /// }
    /// ```
    ///
    /// becomes
    ///
    /// ```c
    /// typedef enum Foo {
    ///     Foo_Bar,
    ///     Foo_Baz,
    /// } Foo;
    /// ```
    pub fn namespace_enums(mut self, switch: bool) -> Header {
        self.namespace_enums = switch;
        self
    }
}

/// A convenience type for people who only want a single C header file.
pub struct Builder(Header, binder::Binder);

impl Builder {
    /// Initialise a compiler which will compile a header file conforming to the C99 standard.
    pub fn c99() -> ::std::io::Result<Builder> {
        Ok(Builder(Header::c99(), try!(binder::Binder::new())))
    }

    /// Initialise a compiler which will compile a header file conforming to a given standard.
    pub fn with_standard(standard: Standard) -> ::std::io::Result<Builder> {
        Ok(Builder(Header::with_standard(standard), try!(binder::Binder::new())))
    }

    /// Give the header file a name.
    ///
    /// This name will be used in the include-guard and will be used to generate the name of the
    /// header file itself.
    pub fn name(mut self, name: &str) -> Builder {
        self.0 = self.0.name(name);
        self
    }

    /// Inject custom code before the generated decalrations.
    ///
    /// The rough structure of the header file will be
    ///
    /// ```c
    /// #ifndef ...
    /// #define ...
    ///
    /// #ifdef ...
    /// extern "C" {
    /// #endif
    ///
    /// #include ...
    ///
    /// <custom code goes here>
    ///
    /// <generated code goes here>
    ///
    /// #ifdef ...
    /// }
    /// #endif
    ///
    /// #endif
    /// ```
    pub fn insert_code(mut self, code: &str) -> Builder {
        self.0 = self.0.insert_code(code);
        self
    }

    /// Prepend the name of enums and an underscore to their variants.
    ///
    /// Defaults to `true`.
    ///
    /// This prevents name clashes in the C code which don't happen in the Rust code due to
    /// namespacing.
    ///
    /// When `true`
    ///
    /// ```no_run
    /// #[repr(C)]
    /// pub enum Foo {
    ///     Bar,
    ///     Baz,
    /// }
    /// ```
    ///
    /// becomes
    ///
    /// ```c
    /// typedef enum Foo {
    ///     Foo_Bar,
    ///     Foo_Baz,
    /// } Foo;
    /// ```
    pub fn namespace_enums(mut self, switch: bool) -> Builder {
        self.0 = self.0.namespace_enums(switch);
        self
    }

    /// Set the path to the root source file of the crate.
    ///
    /// This should only be used when not using a `cargo` build system.
    pub fn source_file<T>(mut self, path: T) -> Builder
        where std::path::PathBuf: std::convert::From<T>,
    {
        self.1.source_file(path);
        self
    }

    /// Set a string to be used as source code.
    ///
    /// Currently this should only be used with small strings as it requires at least one `.clone()`.
    pub fn source_string(mut self, source: &str) -> Builder {
        self.1.source_string(source);
        self
    }

    /// Set the directory in which to place all output bindings.
    ///
    /// This defaults to `target/binder`, and the output from each language will be put in it's own
    /// folder.
    pub fn output_directory<T>(mut self, path: T) -> Builder
        where std::path::PathBuf: std::convert::From<T>,
    {
        self.1.output_directory(path);
        self
    }

    /// Set the module which contains the API which is to be bound.
    ///
    /// The module should be described using Rust's path syntax, i.e. in the same way that you
    /// would `use` the module (`"path::to::api"`).
    ///
    /// # Fails
    ///
    /// If the path is malformed (e.g. `path::to:module`).
    pub fn module(mut self, module: &str) -> ::std::result::Result<Builder, ()> {
        try!(self.1.module(module));
        Ok(self)
    }

    /// Build script utility function for compiling a single header file.
    ///
    /// Compile the header, if there were any errors print them to stderr and panic, otherwise
    /// write the bindings to the file. Then move the bindings
    pub fn run_build(self) {
        use binder::compiler::Compiler;
        let language = self.0.language();
        let name = self.0.name.clone();

        let mut binder = self.1;
        binder.register(self.0);
        binder.run_build();

        let final_dir = binder.output_directory;
        let final_file = final_dir.join(&name);
        let initial_dir = final_dir.join(&language);
        let initial_file = initial_dir.join(&name);
        std::fs::rename(initial_file, final_file)
            .expect("could not move header to parent directory");
        std::fs::remove_dir(initial_dir)
            .expect("could not remove old directory");
    }
}

impl compiler::Compiler for Header {
    fn compile_mod(&mut self, _session: &mut session::Session, module: &syntax::ast::Mod) -> compiler::Result {
        let uses = module.items
            .iter()
            .filter_map(|i| {
                match i.node {
                    syntax::ast::ItemKind::Use(ref p) => Some(p.node.clone()),
                    _ => None,
                }
            });
        self.type_factory.includes = uses.collect();
        Ok(())
    }

    fn compile_trait_item(&mut self, _session: &mut session::Session, _item: &syntax::ast::Item) -> compiler::Result {
        Ok(())
    }
    
    fn compile_ty_item(&mut self, session: &mut session::Session, item: &syntax::ast::Item) -> compiler::Result {
        let mut buffer = String::new();
        push_docs_from_attrs(&mut buffer, &item.attrs, "");

        let name = &item.ident.name.as_str();

        // use the first if we need our own error struct (hopefully not) otherwise the second
        let ty = try!(utils::ty_from_item(session, &item));

        // if the RHS type is generic (which isn't checked at parse) then ignore
        if utils::ty_is_generic(&*ty) { return Err(compiler::Stop::Abort); };

        let type_string = try!(self.type_factory.rust_to_c(&*ty, &name, Some(session)));

        buffer.push_str(&format!("typedef {};\n\n", type_string));

        self.buffer.push_str(&buffer);
        Ok(())
    }

    fn compile_enum_item(&mut self, session: &mut session::Session, item: &syntax::ast::Item) -> compiler::Result {
        let mut buffer = String::new();
        push_docs_from_attrs(&mut buffer, &item.attrs, "");

        let name = &item.ident.name.as_str();

        buffer.push_str(&format!("typedef enum {} {{\n", name));

        let enum_def = try!(utils::enum_from_item(session, &item));

        try!(utils::for_each_variant(session, enum_def, |var| {
            push_docs_from_attrs(&mut buffer, &var.attrs, "\t");

            buffer.push('\t');

            if self.namespace_enums {
                buffer.push_str(&format!("{}_", name));
            }

            buffer.push_str(&format!("{},\n", utils::variant_to_string(&syntax::codemap::spanned(
                syntax::codemap::BytePos(0),
                syntax::codemap::BytePos(0),
                var.clone(),
            ))));
        }));

        buffer.push_str(&format!("}} {};\n\n", name));

        self.buffer.push_str(&buffer);
        Ok(())
    }

    fn compile_struct_item(&mut self, session: &mut session::Session, item: &syntax::ast::Item) -> compiler::Result {
        let mut buffer = String::new();
        push_docs_from_attrs(&mut buffer, &item.attrs, "");

        let name = &item.ident.name.as_str();
        buffer.push_str(&format!("typedef struct {}", name));

        let struct_vars = try!(utils::struct_from_item(session, &item));

        if !utils::is_opaque(struct_vars) {
            buffer.push_str(" {\n");

            for (pos, field) in struct_vars.iter().enumerate() {
                push_docs_from_attrs(&mut buffer, &field.attrs, "\t");

                let name = match field.ident {
                    Some(ref ident) => ident.name.as_str().to_string(),
                    None => format!("_{}", pos),
                };

                let ty = try!(self.type_factory.rust_to_c(&*field.ty, &name, Some(session)));
                buffer.push_str(&format!("\t{};\n", ty));
            }

            buffer.push('}');
        }

        buffer.push_str(&format!(" {};\n\n", name));

        self.buffer.push_str(&buffer);
        Ok(())
    }

    fn compile_fn_item(&mut self, session: &mut session::Session, item: &syntax::ast::Item) -> compiler::Result {
        let mut buffer = String::new();
        push_docs_from_attrs(&mut buffer, &item.attrs, "");

        let fn_decl = try!(utils::fn_from_item(session, &item));

        // Handle the case when the return type is a function pointer (which requires that the
        // entire declaration is wrapped by the function pointer type) by first creating the name
        // and parameters, then passing that whole thing to `rust_to_c`.
        let mut buf_without_return = format!("{}(", &item.ident.name.as_str());

        let has_args = !fn_decl.inputs.is_empty();

        for arg in &fn_decl.inputs {
            let arg_name = try!(utils::arg_name_from_pattern(session, &*arg.pat));
            let arg_type = try!(self.type_factory.rust_to_c(&*arg.ty, &arg_name, Some(session)));
            buf_without_return.push_str(&arg_type);
            buf_without_return.push_str(", ");
        }

        if has_args {
            // Remove the trailing comma and space.
            buf_without_return.pop();
            buf_without_return.pop();

            buf_without_return.push(')');
        } else {
            buf_without_return.push_str("void)");
        }

        let output_type = &fn_decl.output;
        let full_declaration = match *output_type {
            syntax::ast::FunctionRetTy::Default(..) => format!("void {}", buf_without_return),
            syntax::ast::FunctionRetTy::Ty(ref ty) =>
            {
                if utils::ty_to_string(ty) == "!" && self.standard== Standard::C11 {
                    format!("noreturn void {}", buf_without_return)
                }else{
                    try!(self.type_factory.rust_to_c(&*ty, &buf_without_return, Some(session)))
                }
            }
        };

        buffer.push_str(&full_declaration);
        buffer.push_str(";\n\n");

        self.buffer.push_str(&buffer);
        Ok(())
    }

    fn compile_bindings(&self) -> Vec<compiler::File> {
        vec![compiler::File {
            path: std::path::Path::new(&self.name).with_extension("h"),
            contents: wrap_guard(
                &wrap_extern(&format!(
                    "{}\n\n{}\n\n{}",
                    self.standard.includes(),
                    self.custom_code,
                    self.buffer,
                )),
                &self.name,
            ),
        }]
    }

    fn language(&self) -> String {
        format!("c-header-{:?}", self.standard)
    }
}

/// Push documentation onto the given buffer with the format:
///
/// ```c
/// /**
///  * docs go here
///  */
/// ```
///
/// and prepended by a given indent.
fn push_docs_from_attrs(buffer: &mut String, attrs: &[syntax::ast::Attribute], indent: &str) {
        let prepend = format!("{} * ", indent);
        let docs = utils::docs_from_attrs(attrs, &prepend, "");
        if docs.is_empty() { return }

        buffer.push_str(indent);
        buffer.push_str("/**\n");
        buffer.push_str(&docs);
        buffer.push_str(indent);
        buffer.push_str(" */\n");
}

/// Wrap a block of code with an extern declaration.
fn wrap_extern(code: &str) -> String {
    format!(r#"
#ifdef __cplusplus
extern "C" {{
#endif

{}

#ifdef __cplusplus
}}
#endif
"#, code)
}

/// Wrap a block of code with an include-guard.
fn wrap_guard(code: &str, id: &str) -> String {
    format!(r"
#ifndef cheddar_generated_{0}_h
#define cheddar_generated_{0}_h

/**
 * This file was automatically generated by rusty-cheddar.
 * It would probably be wise not to mess with it.
 */

{1}

#endif
", sanitise_id(id), code)
}

/// Remove illegal characters from the identifier.
///
/// This is because macros names must be valid C identifiers. Note that the identifier will always
/// be concatenated onto `cheddar_generated_` so can start with a digit.
fn sanitise_id(id: &str) -> String {
    // `char.is_digit(36)` ensures `char` is in `[A-Za-z0-9]`
    id.chars().filter(|ch| ch.is_digit(36) || *ch == '_').collect()
}


#[cfg(test)]
mod test {
    #[test]
    fn test_sanitise_id() {
        assert!(super::sanitise_id("") == "");
        assert!(super::sanitise_id("!@£$%^&*()_+") == "_");
        // https://github.com/Sean1708/rusty-cheddar/issues/29
        assert!(super::sanitise_id("filename.h") == "filenameh");
    }
}
